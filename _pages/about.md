---
permalink: /
title: "Research profile: Cristian Axenie"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

After earning a PhD in Neuroscience and Robotics from TUM in 2016, I've spent one more year with the TUM Center of Competence Neuroengineering before joining Huawei Research Center in Munich. Since 2017 I am Staff Research Engineer with Huawei's largest research center outside China. At the same time, I lead the Audi Konfuzius-Institut Ingolstadt Lab, a Sino-German research initiative focused on combining modern AI and VR technology for applications ranging from sports technology to biomedical engineering and medical rehabilitation. Each term, I teach AI and ML for undergrads in TH Ingolstadt.
